package info.terrismc.MineBuddy;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class EventListener implements Listener {
	private DataStore dStore;
	
	public EventListener( MineBuddy plugin ) {
		this.dStore = plugin.dStore;
	}
	
	@EventHandler
	public void onPlayerJoin( PlayerJoinEvent event ) {
		// Check if existing player
		Player player = event.getPlayer();
		if( player.hasPlayedBefore() ){
			dStore.popReward( player );
		}
		else {
			dStore.pushReward( player );
		}
	}
}
