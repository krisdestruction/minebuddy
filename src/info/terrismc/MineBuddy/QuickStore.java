package info.terrismc.MineBuddy;

import java.util.Iterator;
import java.util.List;

import org.bukkit.entity.Player;

public class QuickStore {
	private DataStore dStore;
	
	public QuickStore( MineBuddy plugin ) {
		this.dStore = plugin.dStore;
	}

	public void sendBuddyList( Player player ) {
		sendBuddyList( player, player.getName() );
		return;
	}

	public void sendBuddyList( Player playerNotified, String playerName ) {
		List<String> buddyList = dStore.getBuddyList( playerName );
		
		// Change player name to add ownership
		if( playerNotified.getName() == playerName )
			playerName = "your";
		else
			playerName = playerName + "'s";
		
		// Notify player
		if( buddyList.isEmpty() )
			playerNotified.sendMessage( "No players in " + playerName + " buddy list" );
		else {
			playerNotified.sendMessage( "Players in " + playerName + " buddy list:" );
			Iterator<String> it = buddyList.iterator();
			while( it.hasNext() )
				playerNotified.sendMessage( it.next() );
		}
		return;
	}
	
}
