package info.terrismc.MineBuddy;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CommandListener implements CommandExecutor {
	private ConfigStore cStore;
	private DataStore dStore;
	private QuickStore qStore;
	
	public CommandListener( MineBuddy plugin ) {
		cStore = plugin.cStore;
		dStore = plugin.dStore;
		qStore = plugin.qStore;
	}

	@Override
	public boolean onCommand( CommandSender sender, Command cmd, String label, String[] args ) {
		// Check for player
		if( !( sender instanceof Player ) ) {
			sender.sendMessage( "This is a player command" );
			return true;
		}
		
		Player player = (Player) sender;
		if( args.length == 0 ) {
			qStore.sendBuddyList( player );
			return true;
		}
		switch( args[0] ) {
		case "list":
			if( args.length > 1 )
			qStore.sendBuddyList( player, args[1] );
			return true;
		case "invite":
			if( args.length > 1 ) {
				dStore.addBuddyPair( player, player.getName(), args[1] );
				return true;
			}
			return false;
		case "uninvite":
			if( args.length > 1 ) {
				dStore.removeBuddyPair( player, player.getName(), args[1] );
				return true;
			}
			return false;
		case "reload":
			cStore.reload();
			dStore.reload();
			sender.sendMessage( "MineBuddy Config and Data Reloaded" );
			return true;
		}
		return false;
	}

}
