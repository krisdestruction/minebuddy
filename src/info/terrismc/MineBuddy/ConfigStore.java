package info.terrismc.MineBuddy;

import org.bukkit.configuration.file.FileConfiguration;

public class ConfigStore {
	private MineBuddy plugin;
	private FileConfiguration config;
	
	public ConfigStore( MineBuddy plugin ) {
		this.plugin = plugin;
		reload();
	}
	
	public void reload() {
		plugin.saveDefaultConfig();
		plugin.reloadConfig();
		
		config = plugin.getConfig();
	}

	public int getMaxInvites() {
		return config.getInt( "maxInvites", 5 );
	}

	public String getRewardCommand( String playerName ) {
		String rewardCommand = config.getString( "rewardTemplate", null );
		if( rewardCommand != null )
			rewardCommand = rewardCommand.replace( "{player}", playerName );
		return rewardCommand;
	}
}
