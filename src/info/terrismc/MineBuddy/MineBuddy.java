package info.terrismc.MineBuddy;

import java.util.logging.Logger;

import org.bukkit.plugin.java.JavaPlugin;

public class MineBuddy extends JavaPlugin {
	public static Logger logger;
	public ConfigStore cStore;
	public DataStore dStore;
	public QuickStore qStore;
	
	public void onEnable() {
		// Initialize static instances
		logger = this.getLogger();
		
		// Initialize storage instances
		cStore = new ConfigStore( this );
		dStore = new DataStore( this );
		qStore = new QuickStore( this );
		
		// Register events
		this.getServer().getPluginManager().registerEvents( new EventListener( this ), this );
		
		// TODO hook into commands
		this.getCommand( "buddy" ).setExecutor( new CommandListener( this ) );
	}
	
	public void onDisable() {
		dStore.save();
	}
}
