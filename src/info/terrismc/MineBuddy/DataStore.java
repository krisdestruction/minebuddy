package info.terrismc.MineBuddy;

import java.io.File;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

public class DataStore {
	private MineBuddy plugin;
	private ConfigStore cStore;
	private File file;
	private FileConfiguration config;

	public DataStore( MineBuddy plugin ) {
		this.plugin = plugin;
		this.cStore = plugin.cStore;
		
		// Create file
		createFile();
		
		plugin.getServer().getScheduler().runTaskTimerAsynchronously( plugin, new Runnable() {
			public void run() {
				MineBuddy.logger.fine( "Saving data.yml" );
				save();
			}
		}, 0, 1200 );
	}
	
	public void createFile() {
		file = new File( plugin.getDataFolder(), "data.yml" );
		if( !file.exists() ) {
			try {
				file.getParentFile().mkdir();
				file.createNewFile();
			} catch ( Exception e ) {
				MineBuddy.logger.warning( "Cannot create " + file.getName() );
			}
		}
		reload();
	}
	
	public void save() {
		try {
			config.save( file );
		} catch( Exception e ) { 
			MineBuddy.logger.warning( "Cannot save " + file.getName() );
			e.printStackTrace();
		}
	}
	
	public void reload() {
		config = YamlConfiguration.loadConfiguration( file );
	}
	
	public void addBuddyPair( Player player, String playerName, String buddyName ) {
		// Check buddy name
		if( getPlayerName( buddyName ) != null ) {
			player.sendMessage( "This buddy has already been invited" );
			return;
		}
		
		// Check played before
		OfflinePlayer buddyPlayer = Bukkit.getOfflinePlayer( buddyName );
		if( buddyPlayer.hasPlayedBefore() || plugin.getServer().getPlayer( buddyName ) != null ) {
			player.sendMessage( "This buddy has already joined the server" );
			return;
		}
		
		// Check invite limit
		List<String> buddyList = getBuddyList( playerName );
		int maxInvites = cStore.getMaxInvites();
		if( buddyList.size() >= cStore.getMaxInvites() ) {
			player.sendMessage( "You have reached your limit of invites: " + maxInvites );
			return;
		}
		
		// Link buddy to player
		buddyList.add( buddyName );
		setBuddyList( playerName, buddyList );
		
		// Link player to buddy
		setPlayerName( buddyName, playerName );
		
		player.sendMessage( "Buddy invited" );
		return;
	}
	
	public void removeBuddyPair( Player playerNotify, String playerName, String buddyName ) {
		// Check buddy name
		if( playerNotify != null && getPlayerName( buddyName ) == null ) {
			playerNotify.sendMessage( "This buddy has not been invited" );
			return;
		}
		
		// Check bypass permission
		if( playerNotify != null && !playerNotify.hasPermission( "MineBuddy.bypass" ) )
			playerName = getPlayerName( buddyName );
		
		// Check ownership
		List<String> buddyList = getBuddyList( playerName );
		if( playerNotify != null && !buddyList.contains( buddyName ) ) {
			playerNotify.sendMessage( "This buddy was not invited by you" );
			return;
		}
		
		// Unlink buddy to player
		buddyList.remove( buddyName );
		setBuddyList( playerName, buddyList );
		
		// Unlink player to buddy
		this.setPlayerName( buddyName, null );
		
		// Notify uninvite
		if( playerNotify != null )
			playerNotify.sendMessage( "Buddy uninvited" );
		return;
	}

	public List<String> getBuddyList( String playerName ) {
		return config.getStringList( "Players." + playerName );
	}

	private void setBuddyList( String playerName, List<String> buddyList ) {
		config.set( "Players." + playerName, buddyList );
		return;
	}

	private String getPlayerName( String buddyName ) {
		return config.getString( "Buddies." + buddyName );
	}

	private void setPlayerName( String buddyName, String playerName ) {
		config.set( "Buddies." + buddyName, playerName );
		return;
	}

	public void pushReward( Player buddy ) {
		// Check if buddy is a player's buddy
		String buddyName = buddy.getName();
		String playerName = getPlayerName( buddyName );
		if( playerName == null )
			return;
		
		// Get player 
		Player player = plugin.getServer().getPlayer( playerName );
		String rewardCommand = cStore.getRewardCommand( playerName );
		
		// Send or save reward to player
		if( player == null )
			setReward( playerName, rewardCommand );
		else
			sendDelayedReward( rewardCommand );
		
		// Remove buddy player
		removeBuddyPair( null, playerName, buddyName );
	}

	public void popReward( Player player ) {
		String playerName = player.getName();
		String rewardCommand = getReward( playerName );
		if( rewardCommand != null )
			sendDelayedReward( rewardCommand );
		setReward( playerName, null );
	}

	private void sendDelayedReward( final String rewardCommand ) {
		plugin.getServer().getScheduler().runTaskLater( plugin, new Runnable() {
			public void run() {
				Bukkit.dispatchCommand( Bukkit.getConsoleSender(), rewardCommand );
			}
		}, 1 );
		
	}

	private String getReward( String playerName ) {
		return config.getString( "Rewards." + playerName );
	}

	private void setReward( String playerName, String rewardCommand ) {
		config.set( "Rewards." + playerName, rewardCommand );
		return;
	}
}
